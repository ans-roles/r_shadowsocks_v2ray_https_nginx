Role Name
=========
#### shadowsocks_v2ray_https
This role setup a shadowsocks-libev server with v2ray plugin with TLS encryption.  
You have to assign domain name to a host and create TLS certifacete.
The role setup:  
   - shadowsocks-libev
   - v2ray-plugin
   - acl utils (for set file acl permission)  
The role create user for starting server.  
Next, the role set CAPABILITY cap_net_bind_service on v2ray-plugin. This is need for binding port number lower then 1023  
The role set ACL permission to read for server's user for certificate's files.  

This role was testing only on Debian 11  

Requirements
------------

For using this role you need to have domain name and certificates. 


Role Variables
--------------
- shadowsocks_addr       - listen server address (default: 0.0.0.0)  
- shadowsocks_port       - listen server port (default: 443)  
- shadowsocks_encrypt    - encryption method (default: "xchacha20-ietf-poly1305")  
- shadowsocks_timeout    - timeout (default: 600 sec)  
- shadowsocks_nameserver - nameserver (default: 1.1.1.1)  
- shadowsocks_cert_path  - path to the certificate file  (default: role is using path to certificate getting from letsencrypt by - certbot standalone mode "/etc/letsencrypt/live/<virtualhost>/fullchain.pem"   
- shadowsocks_key_path   - path to the private key file  (default: role is using path to certificate getting from letsencrypt by certbot standalone mode "/etc/letsencrypt/live/<virtualhost>/fullchain.pem"  
- shadowsocks_user       - user for server (default: shadowsocks)
- shadowsocks_letsencrypt - is certs and keys located on /etc/letsencrypt/archive/<my_domain.com>/. (defauil: true) If you have another path to certificates then set this to false and ensure you certificates have read permission for *shadowsocks_user*     

Hostvar **virtualhost** must be set 

Dependencies
------------
Nope. 

Example Playbook
----------------
##### inventory.yml
```
   all:
     children:
       servers:
         hosts:
           example.com:
             virtualhost: example.com  - this must be set  
```   
##### playbook.yml
    - hosts: servers
      roles:
        - shadowsocks_v2ray_https

License
-------

BSD

Author Information
------------------
khimick@gmail.com 
